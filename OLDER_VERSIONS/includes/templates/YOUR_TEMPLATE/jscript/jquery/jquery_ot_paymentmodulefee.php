<?php
  $paymentModules = explode(',', MODULE_ORDER_TOTAL_PAYMENTMODULEFEE_PAYMENT_MODULES);
?>
<script type="text/javascript"><!--//
jQuery(document).ready(function() {
  jQuery(document).on('click', 'input[name="payment"]', function() {
    // build array of payment modules
    var paymentsArray = new Array("<?php echo implode('","', $paymentModules); ?>");
    // check for id of payment module in array
    if (typeof(updateForm) === 'function' && jQuery.inArray(jQuery(this).val(), paymentsArray) > -1) {
      updateForm();
    }
  });
});
//--></script>