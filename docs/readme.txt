Payment Module Fee
by Numinix (http://www.numinix.com)

This module allows Zen Cart store's to add a fee or a discount based on payment module selection.  The module is FEC and FEAC ready.

Installation:
=============
- upload all files in the catalog folder to your store root;
- enable the module in ADMIN->MODULES->ORDER TOTAL->PAYMENT MODULE FEE;
- configure the module by following the directions for each configuration setting;

Important Links:
================
Release Announcement Thread: http://numinix.com/forum/viewtopic.php?f=5&t=816
Support Thread: http://www.numinix.com/forum/viewtopic.php?f=5&t=817 